//1. (,) is the first priority
//2. *,/ have the higher priority than +,-

#include <iostream>
#include <vector>
#include <stack>
#include <string>
using namespace std;

bool operators(char c){
if(c=='(' || c==')' || c=='+' || c=='-' || c=='*' || c=='/')
    return true;
else
    return false;
}

int compute(int pre,char ops,int next){
    int ans=0;
    if      (ops=='+')
        ans = pre+next;
    else if (ops=='-')
        ans = pre-next;
    else if (ops=='*')
        ans = pre*next;
    else if (ops=='/')
        ans = pre/next;
    return ans;
}
int main()
{
    char c[50];
    stack<char> num;
    stack<char> op;
    int ans;
    cin>>c;
    int length = strlen(c);
    for(int i=0;i<length;i++){
        char word = c[i];
        if(operators(c[i])==true){
            if(c[i]==')'){
                while (op.top()!='(') {
                    //pop two adjacent numbers
                    char temp = num.top();
                    int next = temp - '0';
                    num.pop();
                    temp = num.top();
                    int pre = temp - '0';
                    num.pop();
                    //pop opertator
                    char ops = op.top();
                    op.pop();
                    ans = compute(pre,ops,next);
                    char s[10];
                    sprintf(s, "%d",ans);
                    num.push(s[0]);
                }
                
            }
            // if c!=')'
            else
            op.push(c[i]);
        
        }
     // c is just a number
     else{
        num.push(c[i]);
         }
        
    }

    if (op.top()=='+'||op.top()=='-'||op.top()=='*'||op.top()=='/') {
        while(!op.empty() && op.top()!='(' ){
            //pop two adjacent numbers
            char temp = num.top();
            int next = temp - '0';
            num.pop();
            temp = num.top();
            int pre = temp - '0';
            num.pop();
            //pop opertator
            char ops = op.top();
            op.pop();
            if(!op.empty())
            char top = op.top();
            ans = compute(pre,ops,next);
            char s[10];
            sprintf(s, "%d",ans);
            num.push(s[0]);

        }
 

    }
    cout<<ans;
    return 0;
}
